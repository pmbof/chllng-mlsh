# PMBOF For MELI - CHALLENGE

Docker:
-------
* Mongo:

> docker pull mongo

> docker run -p 27017:27017 --name meliurl-mongo -d mongo


* Redis:

> docker pull redis

> docker run -p 6379:6379 --name meliurl-redis -d redis redis-server --save 60 1 --loglevel warning

