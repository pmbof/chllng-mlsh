package org.pmbof.algorithm;

import org.pmbof.base.Base36;
import org.pmbof.base.Base62;
import org.pmbof.base.IBase;
import org.pmbof.repository.DomainItem;
import org.pmbof.repository.DomainRepository;
import org.pmbof.repository.UrlItem;
import org.pmbof.repository.UrlRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

public class Algorithm {

    private static final Logger log = LoggerFactory.getLogger(Algorithm.class);

    private static final MessageDigest dgMd5;

    static {
        try {
            dgMd5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private static final MessageDigest dgSHA256;

    static {
        try {
            dgSHA256 = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private final static String _serviceURLbase = "http://pmbof.ddns.net:8081/";

    private final static boolean _bIgnoreCase = false;

    // sensitive:    2^32 = max = 62^n  ->  32 lg 2 = lg max = n lg 62 = 9.6  ->  n = 5.4 ~ 6   -> first 6 digit are for domain
    // un-sensitive: 2^32 = max = 36^n  ->  32 lg 2 = lg max = n lg 36 = 9.6  ->  n = 6.2 ~ 7   -> first 7 digit are for domain
    // Hash for scheme and domain length:
    private final static int _hashUrlDomainLength = _bIgnoreCase ? 7 : 6;

    // Base for convert to String
    private static IBase _coverter2base = _bIgnoreCase ? new Base36() : new Base62();

    // Map: Key: URL long Domains  ->  value: hash
    private static Map<String, String> _byStrlURLDomain = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
    // Map: key: hash   ->  value: URL long Domain
    private static Map<String, String> _byHashDomain = _bIgnoreCase ? new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER) : new TreeMap<String, String>();;

    // Map: Key: URL long  ->  value: hash
    private static Map<String, String> _byStrURL = _bIgnoreCase ? new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER) : new TreeMap<String, String>();;

    // Map: key: hash   ->  value: URL long
    private static Map<String, String> _byHash = _bIgnoreCase ? new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER) : new TreeMap<String, String>();;

    private static boolean _checkCollide = true;


    private DomainRepository domainRepository;
    private UrlRepository urlRepository;

    public Algorithm(DomainRepository domainRepository, UrlRepository urlRepository) {
        this.domainRepository = domainRepository;
        this.urlRepository = urlRepository;
    }


    public String getURLshorten(String strURL) {
        if (strURL == null)
            return null;

        strURL = strURL.trim();
        if (strURL.isEmpty())
            return null;

        String sUrlBase;
        int ischeme = strURL.indexOf("//");
        if (0 < ischeme && ischeme + 2 < strURL.length()) {
            int idom = strURL.indexOf('/', ischeme + 2);
            if (ischeme < idom)
                sUrlBase = strURL.substring(0, idom);
            else
                return null;
        }
        else
            return null;

        String shdom = _byStrlURLDomain.get(sUrlBase);
        if (shdom == null) {
            shdom = hashString(sUrlBase);
            if (shdom.length() < _hashUrlDomainLength)
                shdom = padLeftZeros(shdom, _hashUrlDomainLength);
            else if (_hashUrlDomainLength < shdom.length()) {
                log.error("getURLshorten - hash for domain has " + shdom.length() + " > " + _hashUrlDomainLength + " digits  by domain: " + sUrlBase);
                return null;
            }

            String duplicate = _checkCollide ? _byHashDomain.get(shdom) : null;
            if (duplicate != null)
                log.error("getURLshorten - Collide hash for domain: " + shdom + "\n   -> " + duplicate + "\n   -> " + sUrlBase);
            else {
                _byStrlURLDomain.put(sUrlBase, shdom);
                _byHashDomain.put(shdom, sUrlBase);
            }
        }

        strURL = strURL.substring(sUrlBase.length() + 1);

        String shash = _byStrURL.get(strURL);

        if (shash == null) {
            if (strURL.length() < 45)
                shash = hashString(strURL);
            else if (strURL.length() < 200)
                shash = hashMD5(strURL);
            else
                shash = hashSHA256(strURL);

            String duplicate = _checkCollide ? _byHash.get(shash) : null;
            if (duplicate != null)
                log.error("getURLshorten - Collide hash: " + shash + "\n   -> " + duplicate + "\n   -> " + strURL);
            else {
                _byStrURL.put(strURL, shash);
                _byHash.put(shash, strURL);
            }
        }

        if (domainRepository != null && urlRepository != null) {
            log.debug("getURLshorten - Save domain: " + shdom + " -> " + sUrlBase);
            log.debug("getURLshorten - Save url: " + shash + " -> " + strURL);
            domainRepository.save(new DomainItem(shdom, sUrlBase));
            urlRepository.save(new UrlItem(shash, strURL));
        }
        return _serviceURLbase + shdom + shash;
    }


    public String getURL(String strShorten) {
        if (strShorten == null || strShorten.length() < _serviceURLbase.length() + _hashUrlDomainLength + 1)
            return null;

        int iserver = strShorten.indexOf(_serviceURLbase);
        if (iserver != 0)
            return null;

        return getURLclean(strShorten.substring(_serviceURLbase.length()));
    }



    public String getURLclean(String strShorten) {
        if (strShorten == null || strShorten.length() < _hashUrlDomainLength + 1)
            return null;

        String hashBase = strShorten.substring(0, _hashUrlDomainLength);
        String hash = strShorten.substring(_hashUrlDomainLength);

        String sUrlBase = _byHashDomain.get(hashBase);
        if (sUrlBase == null) {
            Optional<DomainItem> oditem = domainRepository.findById(hashBase);
            if (!oditem.isPresent())
                return null;
            sUrlBase = oditem.get().getDomain();
            _byHashDomain.put(hashBase, sUrlBase);
            _byStrlURLDomain.put(sUrlBase, hashBase);
        }

        String strURL = _byHash.get(hash);
        if (strURL == null) {
            Optional<UrlItem> ouitem = urlRepository.findById(hash);
            if (!ouitem.isPresent())
                return null;
            strURL = ouitem.get().getUrl();
            _byHash.put(hash, strURL);
            _byStrURL.put(strURL, hash);
        }

        return sUrlBase + '/' + strURL;
    }



    public void deleteByShorten(String sUrlShorten) {
        log.debug("deleteByShorten - request: " + sUrlShorten);
        if (sUrlShorten == null || sUrlShorten.length() < _serviceURLbase.length() + _hashUrlDomainLength + 1)
            return;

        int iserver = sUrlShorten.indexOf(_serviceURLbase);
        if (iserver != 0)
            return;

        sUrlShorten = sUrlShorten.substring(_serviceURLbase.length());

        String hashBase = sUrlShorten.substring(0, _hashUrlDomainLength);
        String hash = sUrlShorten.substring(_hashUrlDomainLength);

        String strURL = _byHash.remove(hash);
        if (strURL != null)
            _byStrURL.remove(strURL);

        log.debug("deleteByShorten - removed hash url -> " + hash);
        urlRepository.deleteById(hash);
    }


    public void deleteByURL(String sUrl) {
        if (sUrl == null)
            return;

        sUrl = sUrl.trim();
        if (sUrl.isEmpty())
            return;

        String sUrlBase;
        int ischeme = sUrl.indexOf("//");
        if (0 < ischeme && ischeme + 2 < sUrl.length()) {
            int idom = sUrl.indexOf('/', ischeme + 2);
            if (ischeme < idom)
                sUrlBase = sUrl.substring(0, idom);
            else
                return;
        }
        else
            return;

        String shdom = _byStrlURLDomain.get(sUrlBase);
        if (shdom == null)
                return;

        sUrl = sUrl.substring(sUrlBase.length() + 1);

        String urlHash = _byStrURL.remove(sUrl);
        if (urlHash != null) {
            log.debug("deleteByURL - removed hash url: " + urlHash);
            _byHash.remove(urlHash);
            urlRepository.deleteById(urlHash);
        } else {
            log.debug("deleteByURL - removing hash url [" + sUrl + "]");
            // Warning, this method performs a full scan:
            UrlItem ouitem = urlRepository.findItemByUrl(sUrl);
            if (ouitem != null) {
                log.debug("deleteByURL - removing hash url [" + sUrl + "]: " + ouitem.getHash());
                urlRepository.deleteById(ouitem.getHash());
            }
        }
    }



    public static void main(String[] args) throws NoSuchAlgorithmException {

        Algorithm alg = new Algorithm(null, null);

        String[] sPaths = new String[] {
                "https://eletronicos.mercadolivre.com.br/seguranca-casa/#menu=categories",
                "https://console-openshift-console.apps.ocpnoprod.correo.local/console/login/LoginForm.jsp",
                "http://jboss-dc.correo.local:9990/console/App.html#home",
                "COMP/REST/SI_ao_RecibeSMS?MSISDN=27&MessageID=9647852141&Date=24062019100034&Status=Accepted&StatusCode=01&Description=Accepted&DescriptionCode=01",
                "COMP/REST/SI_ao_RecibeSMS?MessageID=9&Date=240620191000&Status=Accepted&Description=Accepted",
                "ui/v1/signin",
                "users/sign_in",
                "apiTiendaNube/api/TiendaNubeApi/GetBrachOffices",
                "eft/process/procCopy.php?intId=170",
                "eft.new.test/process/procCopy.php?intId=170",
                "index2.html",
                "ConsumoJano/services/rest/monitor/testtoken",
                "ConsumoMulticliente/WSMulticlient?wsdl",
                "ApiCotizador/services/rest/v1/apiTiendaNube/getUser",
                "ConsumoMulticliente/WSMulticlient?wsdl",
                "api",
                "console/App.html#home",
                "WSLoggerBean/LoggerBean?wsdl",
                "MiCorreo/public/inicio/index.html",
                "login",
                "demostore/?ref=menu",
                "apiTiendaNube/TiendaNube/AddNewOrders?locale=es&store=1642976&id[]=444890435",
                "MiCorreo/public/login",
                "tiendanube/AddNewOrders?locale=es&store=1567520&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578",
                "apiTiendaNube/tiendanube/AddNewOrders?locale=es&store=1567520&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578",
                "APITIENDANUBE/tiendanube/AddNewOrders?locale=es&store=1567520&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578&id[]=532036578",
                "EMeliBeanService/ExteriorizacionMeli?wsdl"
        };

        Map<String, String> mhashString = new HashMap<String, String>();
        Map<String, String> mhashMD5 = new HashMap<String, String>();
        Map<String, String> mhashSHA256 = new HashMap<String, String>();

        for (int i =0; i < sPaths.length; ++i) {
            boolean bErr = false;

            long t0 = System.currentTimeMillis();
            String strShorten = alg.getURLshorten(sPaths[i]);
            if (strShorten != null)
                System.out.println(i + ". elapsed time " + (System.currentTimeMillis() - t0) + "ms [" + sPaths[i].length() + ", " + strShorten.length() + "]:\n"
                        + "    URL : " + sPaths[i] + "\n"
                        + "    shrt: " + strShorten);
            else
                System.err.println(i + ". elapsed time " + (System.currentTimeMillis() - t0) + "ms [" + sPaths[i].length() + ", 0]:\n"
                        + "    URL : " + sPaths[i] + "\n"
                        + "    shrt: Error generating hash");

            String urlLong = alg.getURL(strShorten);
            if (!sPaths[i].equals(urlLong))
                System.err.println(i + ". elapsed time " + (System.currentTimeMillis() - t0) + "ms Error inverse getting URL long: from " +strShorten + " -> " + sPaths[i]);

            if (strShorten != null) {
                strShorten = strShorten.substring(_serviceURLbase.length());
                String urlLongc = alg.getURLclean(strShorten);
                if (!sPaths[i].equals(urlLong))
                    System.err.println(i + ". elapsed time " + (System.currentTimeMillis() - t0) + "ms Error inverse getting URL long: from clean " + strShorten + " -> " + sPaths[i]);
            }
        }

        for (int i = 62 * 62; i < 62 * 62 * 62 + 1; ) {
            String ln = "";
            for (int j = i + 62; i < j; ++i) {
                if (j - 62 < i)
                    ln += "\t";
                ln += i + "=" + alg._coverter2base.convertInt2((i));
            }
            System.out.println(ln);
        }
    }



    private String padLeftZeros(String str, int length) {
        StringBuilder sb = new StringBuilder();

        while (sb.length() < length - str.length())
            sb.append('0');

        sb.append(str);
        return sb.toString();
    }


    private String hashString(String str) {
        return _coverter2base.convertInt2(str.hashCode());
    }


    private String hashMD5(String str) {
        byte[] enhashMd5 = dgMd5.digest(str.getBytes(StandardCharsets.UTF_8));
        return _coverter2base.convertHexa2(enhashMd5);
    }


    private String hashSHA256(String str) {
        byte[] enhashSHA256 = dgSHA256.digest(str.getBytes(StandardCharsets.UTF_8));
        return _coverter2base.convertHexa2(enhashSHA256);
    }

}
