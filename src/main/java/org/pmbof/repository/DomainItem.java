package org.pmbof.repository;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Objects;


@Document("hashdomain")
public class DomainItem {
    private @Id String hash;
    private String domain;


    public DomainItem() {}

    public DomainItem(String hash, String domain) {
        this.hash = hash;
        this.domain = domain;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof DomainItem))
            return false;
        DomainItem dbo = (DomainItem) o;
        return Objects.equals(this.hash, dbo.hash) && Objects.equals(this.domain, dbo.domain);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.hash, this.domain);
    }


    @Override
    public String toString() {
        return "Domain: {" + "hash=" + this.hash + ", domain=\"" + this.domain + "\"}";
    }

}
