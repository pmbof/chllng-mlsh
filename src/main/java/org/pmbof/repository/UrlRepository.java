package org.pmbof.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;


public interface UrlRepository extends MongoRepository<UrlItem, String> {

    @Query("{url:'?0'}")
    UrlItem findItemByUrl(String url);
}
