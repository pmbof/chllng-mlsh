package org.pmbof.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class Load {
    private static final Logger log = LoggerFactory.getLogger(Load.class);

    protected String getDatabaseName() {
        return "meliurlshort";
    }


    @Bean
    CommandLineRunner initDatabase(UrlRepository repository) {
       return args -> {
       };
    }
}
