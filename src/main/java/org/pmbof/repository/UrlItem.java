package org.pmbof.repository;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;


@Document("hashurl")
public class UrlItem {

    private @Id String hash;
    private String url;


    public UrlItem() {}

    public UrlItem(String hash, String url) {
        this.hash = hash;
        this.url = url;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof UrlItem))
            return false;
        UrlItem dbo = (UrlItem) o;
        return Objects.equals(this.hash, dbo.hash) && Objects.equals(this.url, dbo.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.hash, this.url);
    }


    @Override
    public String toString() {
        return "DbUrl: {" + "hash=" + this.hash + ", url=\"" + this.url + "\"}";
    }

}
