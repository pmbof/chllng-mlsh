package org.pmbof.repository;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface DomainRepository extends MongoRepository<DomainItem, String> {
}
