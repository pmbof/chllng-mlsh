package org.pmbof.controller;

public class UrlNotFoundException extends RuntimeException {

    UrlNotFoundException() {
        super("Could not find shorten URL");
    }

}
