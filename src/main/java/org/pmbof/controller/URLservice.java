package org.pmbof.controller;

import org.pmbof.algorithm.Algorithm;

import org.pmbof.beans.Url;
import org.pmbof.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;


@RestController
public class URLservice {

    private static final Logger log = LoggerFactory.getLogger(URLservice.class);

    //@Resource
    private Algorithm alg;


    URLservice(DomainRepository domainRepository, UrlRepository urlRepository) {
        alg = new Algorithm(domainRepository, urlRepository);
    }


    @PostMapping("/URLshorten")
    public Url getURLshorten(@RequestBody Url jsURL) {
        log.info("getURLshorten - request: " + jsURL.getUrl());

        Url shortUrl = new Url();
        shortUrl.setUrl(alg.getURLshorten(jsURL.getUrl()));

        if (shortUrl.getUrl() != null)
            log.info("getURLshorten - response: " + shortUrl.getUrl());
        else
            log.error("getURLshorten - can not generate short URL");

        return shortUrl;
    }

    @PostMapping("/URL")
    public Url getURL(@RequestBody Url sURLshorten) {
        log.info("getURL - request: " + sURLshorten.getUrl());

        Url longUrl = new Url();
        longUrl.setUrl(alg.getURL(sURLshorten.getUrl()));


        if (longUrl.getUrl() == null) {
            log.error("getURL - can not found short URL");
            throw new UrlNotFoundException();
        }
        log.info("getURL - response: " + longUrl.getUrl());

        return longUrl;
    }


    @GetMapping("/URL/{short}")
    public String getURLclean(@PathVariable("short") String sURLshorten) {
        log.info("getURLclean - URL/" + sURLshorten);

        Url longUrl = new Url();
        String url = alg.getURLclean(sURLshorten);
        if (url != null)
            log.info("getURLclean - response: " + url);
        else
            log.error("getURLclean - not found");

        return url;
    }


    @DeleteMapping("/URLshorten")
    void deleteURLshorten(@RequestBody Url djsURL) {
        log.info("deleteURLshorten - request: " + djsURL.getUrl());

        alg.deleteByShorten(djsURL.getUrl());

        log.info("deleteURLshorten - end");
    }


    @DeleteMapping("/URL")
    void deleteURL(@RequestBody Url djsURL) {
        log.info("deleteURL - request: " + djsURL.getUrl());

        alg.deleteByURL(djsURL.getUrl());

        log.info("deleteURL - end");
    }


}
