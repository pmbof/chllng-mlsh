package org.pmbof;


import org.pmbof.repository.DomainRepository;
import org.pmbof.repository.UrlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Challenge implements CommandLineRunner {

    @Autowired
    UrlRepository urlRepository;

    @Autowired
    DomainRepository domainRepository;

    public static void main(String[] args) {
        SpringApplication.run(Challenge.class, args);
    }

    public void run(String... args) {
    }

}
