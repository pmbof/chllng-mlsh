package org.pmbof.base;

import java.math.BigInteger;
import java.nio.ByteBuffer;

public class Base36 implements IBase {

    // from 0 to 9 = 10 symbols
    // from A to Z = 26 symbols
    // Total       = 36 symbols

    static private final int _nBase = 36;
    static private final boolean _bLowercase = false;

    static private final BigInteger _binBase = BigInteger.valueOf(_nBase);
    static private char _iniChar = _bLowercase ? 'a' : 'A';


    @Override
    public String convertInt2(int ival) {
        byte[] bytes = ByteBuffer.allocate(4).putInt(ival).array();
        return convertHexa2(bytes);
    }

    @Override
    public String convertStrHexa2(String shexa) {
        // 36 / 16 = 2.25   -> 2(1/4)B   72 bites

        byte[] bhexa = shexa.getBytes();
        String svret = "";

        BigInteger value = BigInteger.ZERO;
        BigInteger fcHexa = BigInteger.ONE;
        int ibyte = 0;
        char boffset = 3;
        for (int i = bhexa.length - 1; 0 <= i; --i, ++ibyte) {
            if (ibyte % 3 == 0) {
                if (i < bhexa.length - 1) {
                    boffset <<= 2;
                    if (boffset == 0) {
                        svret = convert36(value) + svret;

                        boffset = 3;
                        ibyte = 0;

                        value = BigInteger.ZERO;
                        fcHexa = BigInteger.ONE;
                    }
                }
            }

            BigInteger byteval;
            if ('a' <= bhexa[i] && bhexa[i] <= 'f')
                byteval = fcHexa.multiply(BigInteger.valueOf(10 + bhexa[i] - 'a'));
            else if ('A' <= bhexa[i] && bhexa[i] <= 'F')
                byteval = fcHexa.multiply(BigInteger.valueOf(10 + bhexa[i] - 'A'));
            else
                byteval = fcHexa.multiply(BigInteger.valueOf(bhexa[i] - '0'));

            value = value.add(byteval);
            fcHexa = fcHexa.multiply(BigInteger.valueOf(16));
        }
        svret = convert36(value) + svret;

        return svret;
    }

    @Override
    public String convertHexa2(byte[] bhexa) {
        // 36 / 16 = 2.25   -> 2(1/4)B   72 bites

        String svret = "";

        BigInteger value = BigInteger.ZERO;
        BigInteger fcHexa = BigInteger.ONE;
        int ibyte = 0;
        char boffset = 3;
        for (int i = bhexa.length - 1; 0 <= i; --i, ++ibyte) {
            if (ibyte % 3 == 0) {
                if (i < bhexa.length - 1) {
                    boffset <<= 2;
                    if (boffset == 0) {
                        svret = convert36(value) + svret;

                        boffset = 3;
                        ibyte = 0;

                        value = BigInteger.ZERO;
                        fcHexa = BigInteger.ONE;
                    }
                }
            }

            BigInteger byteval = fcHexa.multiply(BigInteger.valueOf(bhexa[i] < 0 ? 256 + (int)bhexa[i] : (int)bhexa[i]));
            value = value.add(byteval);
            fcHexa = fcHexa.multiply(BigInteger.valueOf(256));
        }
        svret = convert36(value) + svret;

        return svret;
    }


    private static String convert36(BigInteger value) {
        String svret = "";

        while (!value.equals(BigInteger.ZERO)) {
            BigInteger[] dr = value.divideAndRemainder(_binBase);
            int remainder = dr[1].intValue();
            if (remainder < 10)
                svret = (char)('0' + remainder) + svret;
            else
                svret = (char)(_iniChar + (remainder - 10)) + svret;

            value = dr[0];
        }

        return svret;
    }


    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

}
