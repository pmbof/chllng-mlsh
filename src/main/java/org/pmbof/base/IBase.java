package org.pmbof.base;

public interface IBase {

    // Convert from int 32 to base as string
    public String convertInt2(int ival);

    // Convert from string in hexadecimal to base as string
    public String convertStrHexa2(String shexa);

    // Convert from binary to base as string
    public String convertHexa2(byte[] bhexa);

}
